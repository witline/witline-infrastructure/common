# A repository for common tasks
... meant to facilitate other microservices in this application.

It handles:
* Commonly used configmaps
* Commonly used secrets - through Gitlab CI variables - not stored in the repo files. (to do)
* Common `.gitlab-ci-<name>-template.yml` files which are used in other projects. Check the `ci-templates` directory in this repository.


## How to use this repository?
* To add any configmaps simply add a yaml file for it in the `configmaps/` directory. The file must have a `.yaml` suffix (file-extension). The namespace where you want your configmaps to be created / updated is defined in the `.gitlab-ci.yaml` file. Commit your changes, and push to upstream. The CI will apply the changes automatically. 

* To introduce steps, or stages, or to fix something in common gitlab ci template files, edit the related gitlab ci template file under `ci-templates` directory. Commit your changes, and push to upstream.


