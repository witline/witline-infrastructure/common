#!/bin/bash
echo
echo "Usage: $0 <namespace>"
if [ -z "$1" ]; then
  echo "Please provide the name of NAMESPACE to use."
  echo "The ./configmaps/ directory will be checked for YAML files and will be applied."
  echo "Exiting ..."
  exit 1
fi

NAMESPACE=$1

for FILE in configmaps/*.yaml; do
  echo "Processing configmap file - ${FILE} - inside namespace - ${NAMESPACE}"
  echo "-- Running - kubectl --namespace=${NAMESPACE} delete -f ${FILE}"
  kubectl --namespace=${NAMESPACE} delete -f ${FILE} || true
  echo "-- Running - kubectl --namespace=${NAMESPACE} apply -f ${FILE}"
  kubectl --namespace=${NAMESPACE} apply -f ${FILE}
done
