#!/bin/bash
echo
echo "Usage: $0 <namespace>  <secret-name>  <path-and-name-of-secrets-file> "
if [ -z "$1" ]; then
  echo "Please provide the name of NAMESPACE to use. Exiting ..."
  exit 1
else
  NAMESPACE=$1
fi

if [ -z "$2" ]; then
  echo "Please provide the name of the secret to create. (all lowercase, alpha-numeric)"
  echo " Exiting ..."
  exit 1
else
  K8S_SECRET_NAME=$2
fi

if [ -z "$3" ]; then
  echo "Please provide the full path of the secrets file to use. Exiting ..."
  exit 1
else
  ENV_FILE=$3
fi


############################ END - USER configurable section ###########

if [ ! -r ${ENV_FILE} ]; then
  echo "File ${ENV_FILE} is not readable . Exiting ..."
  exit 1
fi

echo
echo "Processing secrets file: ${ENV_FILE}..."

NAMESPACE_OPTION="--namespace=${NAMESPACE}"

K8S_COMMAND="kubectl ${NAMESPACE_OPTION} create secret generic ${K8S_SECRET_NAME} "

TEMP_FILE=$(mktemp)
# Clean up the file by removing comments and empty lines.
#   Then, only include lines with '=' present in them. 
egrep -v '\#|^$' ${ENV_FILE} | grep '=' > ${TEMP_FILE}

# Build the full command to create secret.

echo

while IFS= read -r LINE
do
  VARIABLE_NAME=$(echo ${LINE} | cut -d '=' -f1)
  echo "Adding variable: ${VARIABLE_NAME} ..."
  K8S_COMMAND="${K8S_COMMAND} --from-literal=${LINE}"  
done < ${TEMP_FILE}

echo

# echo "DEBUG: complete command: ${K8S_COMMAND}"


# source ${ENV_FILE} 

echo "Deleting secret - ${K8S_SECRET_NAME} if already exists in namespace - ${NAMESPACE} ..."
echo

kubectl ${NAMESPACE_OPTION} delete secret ${K8S_SECRET_NAME} || true

echo
echo "Creating secret - ${K8S_SECRET_NAME} - inside namespace - ${NAMESPACE}"
echo

# Execute the command we built in the while loop above.


${K8S_COMMAND}

echo

#~ kubectl --namespace=${NAMESPACE} \
  #~ create secret generic ${K8S_SECRET_NAME} \
  #~ --from-literal=JWT_API_KEY=${JWT_API_KEY} \
  #~ --from-literal=JWT_AUDIENCE=${JWT_AUDIENCE} \
  #~ --from-literal=JWT_ISSUER=${JWT_ISSUER} \
  #~ --from-literal=RABBITMQ_DEFAULT_USER=${RABBITMQ_DEFAULT_USER} \
  #~ --from-literal=RABBITMQ_DEFAULT_PASS=${RABBITMQ_DEFAULT_PASS} \
  #~ --from-literal=SENDGRID_API_KEY=${SENDGRID_API_KEY} \
  #~ --from-literal=STRIPE_PUBLISHABLEKEY=${STRIPE_PUBLISHABLEKEY} \
  #~ --from-literal=STRIPE_SECRET_KEY=${STRIPE_SECRET_KEY} \
  #~ --from-literal=TWILIO_ACCOUNT_ID=${TWILIO_ACCOUNT_ID} \
  #~ --from-literal=TWILIO_AUTH_TOKEN=${TWILIO_AUTH_TOKEN} \
  #~ --from-literal=MONGO_APP_USERNAME=${MONGO_APP_USERNAME} \
  #~ --from-literal=MONGO_APP_PASSWORD=${MONGO_APP_PASSWORD}
